import streamlit as st

def deteksi_penyakit(gejala):
    penyakit = ""
    obat = ""

    if gejala == "Panas":
        penyakit = "Demam"
        obat = "Paracetamol, Aspirin, dan Ibuprofen"
    elif gejala == "Pusing":
        penyakit = "Migrain"
        obat = "Bodrex, Bodrex Migrain, Alprazolam, Betahistine"
    elif gejala == "Batuk":
        penyakit = "Batuk pilek"
        obat = "Procold Flu & Batuk, OBH Combi Batuk dan Pilek, dan Viks Formula 44"
    elif gejala == "Sakit Perut":
        penyakit = "Lambung"
        obat = "Promagh, Mylanta, dan Nexium"   
    elif gejala == "Luka":
        penyakit = "Kulit"
        obat = "Hidrokortison, Calamine, dan Crotamiton"  
    else:
        penyakit = "Penyakit tidak terdeteksi"
        obat = "Tidak ada rekomendasi obat"

    return penyakit, obat

st.title("Aplikasi Deteksi Penyakit")
gejala_input = st.selectbox("Pilih gejala:", ["Panas", "Pusing", "Batuk", "Sakit Perut", "Luka"])
penyakit, obat = deteksi_penyakit(gejala_input)
st.write("Penyakit yang terdeteksi:", penyakit)
st.write("Rekomendasi obat:", obat)
