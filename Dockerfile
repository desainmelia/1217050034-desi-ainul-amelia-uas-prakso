# Base Image
FROM debian:bullseye-slim

#install
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

#Copy web page files
COPY gejala.py /home/gejala/gejala.py
COPY requirements.txt /home/gejala/requirements.txt

#install python
RUN pip3 install --no-cache-dir -r /home/gejala/requirements.txt

#set working directory
WORKDIR /home/gejala

#expose port
EXPOSE 25034

#start web page
CMD ["streamlit", "run", "--server.port=25034", "gejala.py"]
