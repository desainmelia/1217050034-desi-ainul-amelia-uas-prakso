# Deskripsi Project
WEB PAGE Layanan Deteksi Penyakit

![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-uas-prakso/-/raw/main/Dokumentasi/Screenshot__353_.png)

Layanan Deteksi Penyakit ini saya buat karena biasanya ketika anak kos sakit, suka bingung untuk mendiagnosa dan membeli obat apa yang cocok ketika badan tidak sehat. Web Page ini membantu anak kos untuk bisa mendeteksi dan memilih obat yang sesuai dengan penyakit yang dirasakan.

Ketika memilih penyakitnya, misalkan pusing, maka web ini akan mendiagnosa penyakitnya dan menyarankan obat berdasarkan penyakitnya.

# Penjelasan Bagaimana Sistem Operasi Digunakan Dalam Proses Containerization

Sistem Operasi berperan penting dalam proses Containerization, karena dalam prosesnya sistem operasi disebut sebagai "Base Image". Base Image merupakan sistem operasi yang digunakan sebagai dasar untuk membuat kontainer, Base Image berisi sistem operasi yang telah dikonfigurasi dengan paket perangkat lunak dan dependensi yang dibutuhkan.

Base image yang digunakan pada web page ini adalah linux yang diambil dari docker hub. Base Image juga terdiri dari beberapa lapisan, Setiap perubahan pada kontainer, seperti menyalin file, menginstal paket, atau menjalankan perintah, ditambahkan sebagai lapisan terpisah di atas base image. Lapisan-lapisan ini dibuat secara efisien dan dapat di-cache untuk mempercepat proses pembuatan dan pengiriman kontainer. Sistem operasi yang digunakan dalam kontainer berinteraksi dengan container runtime, seperti Docker, untuk membuat, menjalankan, dan mengelola kontainer.

# Penjelasan Bagaimana Containerization Dapat Membantu Mempermudah Pengembangan Aplikasi

Dengan menggunakan Containerization, kita dapat mengakses web/program/aplikasi dan dependensinya dalam satu tempat host. ini membantu pengembang agar tidak terjadi perbedaan kondigurasi yang disebabkan oleh perbedaan tempat hostnya. 

Penggunaan alat manajemen dependensi seperti docker compose memudahkan pengembang untuk mengatur dan mengelola dependensi aplikasi dengan mudah. dan dengan Containerization pengembang dapat mempercepat proses pengembangan dan pengiriman aplikasi.

# Penjelasan Apa itu DevOps, bagaimana DevOps Membantu Pengembangan Aplikasi

DevOps merupakan pendekatan kolaboratif dalam pengembangan perangkat lunak yang menggabungkan praktik-praktik dari pengembangan (Development) dan operasional (Operations). DevOps bertujuan untuk meningkatkan kerjasama Dengan adanya kerjasama yang baik, tim pengembang dan operasional dapat saling memahami kebutuhan dan tantangan masing-masing, sehingga mempercepat proses pengembangan dan pengiriman aplikasi, efisiensi, dan kualitas dalam pengembangan, pengujian, dan pengiriman aplikasi.  DevOps memungkinkan pengembang untuk menghasilkan aplikasi yang lebih stabil, scalable, dan mudah dikelola di berbagai lingkungan produksi. 

# Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata (contoh bagaimana implementasi DevOps di Gojek, dsb.)

Berikut merupakan salah satu penerapan DevOps di aplikasi Satu Sehat, dimana Satu Sehat menerapkan Tim Terpadu yang terdiri dari pengembang perangkat lunak, profesional operasional IT dan anggota dari tim bisnis. yang dimana akan bekerja sama untuk mengembangkan dan mengkomunikasikan aplikasi secara terbuka untuk memahami kebutuhan pengguna dan mengintegrasikan perspektif operasional ke dalam pengembangan.

Satu Sehat menerapkan praktik Continuous Integration dan Deployment. Setiap kali ada perubahan kode, mereka melakukan integrasi secara teratur untuk memastikan kompatibilitas dan menguji aplikasi secara otomatis. Selanjutnya, aplikasi yang lulus uji otomatis dikirimkan secara otomatis ke lingkungan produksi.

# Demonstrasi Project Web Page Layanan Deteksi Penyakit
https://youtu.be/6gG8879h-vo
# Mempublikasikan Docker Image yang telah dibuat ke Docker Hub
https://hub.docker.com/repository/docker/dsnmel/gejala

